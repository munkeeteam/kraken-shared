package types

import "time"

// Regions
type Region struct {
	RegionId    int    `gorm:"column:id"`
	Name        string `gorm:"column:name"`
	Description string `gorm:"column:description"`
}

func (Region) TableName() string {
	return "krkn.regions"
}

//Businesses
type Business struct {
	BusinessId int    `gorm:"column:id"`
	Name       string `gorm:"column:name"`
	RegionId   int    `gorm:"column:region_id"`
}

func (Business) TableName() string {
	return "krkn.businesses"
}

//Competitors
type Competitor struct {
	Id          int    `gorm:"column:id"`
	Name        string `gorm:"column:name"`
	Description string `gorm:"column:description"`
	Domain      string `gorm:"column:domain"`
	Enabled     bool   `gorm:"column:enabled"`
	Rules       string `gorm:"column:rules"`
}

func (Competitor) TableName() string {
	return "krkn.competitors"
}

//Departments
type Department struct {
	Id           int    `gorm:"column:id"`
	Name         string `gorm:"column:name"`
	CompetitorId int    `gorm:"column:competitor_id"`
}

func (Department) TableName() string {
	return "krkn.departments"
}

//Categories
type Category struct {
	Id           int    `gorm:"column:id"`
	Name         string `gorm:"column:name"`
	DepartmentId int    `gorm:"column:department_id"`
	Enabled      bool   `gorm:"column:enabled"`
	Url          string `gorm:"column:start_url"`
}

func (Category) TableName() string {
	return "krkn.categories"
}

//Extractions
type Extraction struct {
	CategoryId  int       `gorm:"column:category_id" json:"id"`
	ExtractedAt time.Time `gorm:"column:extracted_at" json:"extracted_data"`
	Data        string    `gorm:"column:data;type:jsonb" json:"data"`
}

func (Extraction) TableName() string {
	return "krkn.extracted_data_temp"
}

//Matches
type Match struct {
	CompetitorId int    `gorm:"column:competitor_id"`
	UPC          string `gorm:"column:upc"`
	SKU          string `gorm:"column:sku"`
	Url          string `gorm:"column:url"`
}

func (Match) TableName() string {
	return "krkn.matches"
}

// Tentacles
type Tentacle struct {
	Id           int    `gorm:"column:id"`
	CompetitorId int    `gorm:"column:competitor_id"`
	Name         string `gorm:"column:mod_name"`
	Shasum       string `gorm:"column:shasum"`
}

func (Tentacle) TableName() string {
	return "krkn.tentacles"
}

