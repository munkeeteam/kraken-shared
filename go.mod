module bitbucket.org/munkeeteam/kraken-shared

go 1.12

require (
	github.com/EDDYCJY/fake-useragent v0.2.0
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8 // indirect
	github.com/go-kit/kit v0.9.0
	github.com/gorilla/mux v1.7.3
	github.com/mafredri/cdp v0.24.2
	github.com/mailgun/mailgun-go/v3 v3.6.0
	github.com/peterbourgon/diskv v2.0.1+incompatible // indirect
	github.com/pkg/errors v0.8.1
	github.com/segmentio/ksuid v1.0.2
	github.com/slotix/dataflowkit v0.0.0-20190411135809-afc0d3fe495f
	github.com/spf13/viper v1.4.0
	go.uber.org/zap v1.10.0
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	golang.org/x/text v0.3.2
)
