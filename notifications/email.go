package notifications

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"time"

	"github.com/mailgun/mailgun-go/v3"
	"github.com/pkg/errors"
)

const (
	DEFAULT_FROM    = "Kraken <no-reply@kraken.wtf>"
	DEFAULT_SUBJECT = "Kraken Notification"
	MG_DOMAIN       = "mg.kraken.wtf"
	MG_APIKEY       = "e191e723fd200e924ea88dbfa5c8f166-2b0eef4c-d6d21446"
)

func SendEmail(recipients []string, subject string, from string, templateFile string, data map[string]interface{}) (bool, error) {
	// Validations
	if len(recipients) == 0 {
		return false, errors.New("need at least one recipient")
	}
	if subject == "" {
		subject = DEFAULT_SUBJECT
	}
	if from == "" {
		from = DEFAULT_FROM
	}
	if templateFile == "" {
		return false, errors.New("need a template")
	}

	mg := mailgun.NewMailgun(MG_DOMAIN, MG_APIKEY)
	m := mg.NewMessage(from, subject, subject)

	// Prepare template
	var b bytes.Buffer
	templateFile = fmt.Sprintf("./templates/%s", templateFile)
	tmpl := template.Must(template.ParseFiles(templateFile))
	err := tmpl.Execute(&b, data)
	if err != nil {
		return false, errors.New(err.Error())
	}
	m.SetHtml(b.String())

	// Adding recipients
	for _, recipient := range recipients {
		m.AddRecipient(recipient)
	}

	// Send the message
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()
	_, id, err := mg.Send(ctx, m)
	if err != nil {
		return false, err
	}
	fmt.Println(id)
	return true, nil
}
