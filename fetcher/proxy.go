package fetcher

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"time"
)

type Proxy struct {
	// Proxy Scheme : HTTP, HTTPS
	Scheme string
	// Proxy URL: IP:PORT
	URL string
	// Proxy username
	User string
	// Proxy password
	Password string
}

func GetProxy() string {
	proxies, _ := readProxies()
	rand.Seed(time.Now().Unix())
	proxy := proxies[rand.Intn(len(proxies))]
	return fmt.Sprintf("%s://%s", proxy.Scheme, proxy.URL)
}

func readProxies() ([]Proxy, error) {
	var proxies []Proxy

	// Open the file
	csvfile, err := os.Open("proxylist.csv")
	if err != nil {
		log.Fatalln("Couldn't open the proxies file", err)
	}

	// Parse the file
	r := csv.NewReader(csvfile)

	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		proxy := Proxy{
			Scheme:   "http",
			URL:      record[0] + ":" + record[1],
			User:     record[2],
			Password: record[3],
		}
		proxies = append(proxies, proxy)
	}
	return proxies, nil
}
