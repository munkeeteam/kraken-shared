# kraken-shared

kraken-shared is a collection of packages for Kraken project.

`import "bitbucket.org/munkeeteam/kraken-shared/types"`

### limiter
limit the number of concurrent go routines to 10:

`import "bitbucket.org/munkeeteam/kraken-shared/limiter"`
