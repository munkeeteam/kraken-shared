package autoreports

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"text/template"
	"time"

	"github.com/segmentio/ksuid"
)

func CreateAutoReport(view, startdate, enddate, delimiter, workingPath string, useTxt bool) (e error) {
	var reportExtension, reportDelimiter string

	randomFilename := ksuid.New().String()

	//TODO: get db information from secure method
	connectionString := fmt.Sprintf("postgresql://%s:%s@%s:%s/%s",
		"wm_pricing", "hkh2n8ws2q7a9bgw", "kraken-db-pg-do-user-6334004-0.db.ondigitalocean.com", "25061", "wm_pricing_pool")

	switch delimiter {
	case "CSV":
		reportDelimiter = "','"
		reportExtension = "csv"
	case "TSV":
		reportDelimiter = "E'\t'"
		if !useTxt {
			reportExtension = "tsv"
		} else {
			reportExtension = "txt"
		}
	}

	data := map[string]interface{}{
		"bucketname": "s3://muscache-kraken/downreports/",
		"reportname": randomFilename,
		"delimiter":  reportDelimiter,
		"extension":  reportExtension,
		"connection": connectionString,
		"view":       view,
		"startdate":  startdate,
		"enddate":    enddate,
		"body":       "Your report is ready to download",
		"date":       time.Now().Format("02-Jan-2006"),
	}

	var b bytes.Buffer
	// TODO: rename template file
	templateFile := fmt.Sprintf("./templates/%s", "report.template")

	tmpl := template.Must(template.ParseFiles(templateFile))
	err := tmpl.Execute(&b, data)
	if err != nil {
		return err
	}

	// write the whole body at once
	fullPath := fmt.Sprintf("%s/%s", workingPath, randomFilename)
	err = ioutil.WriteFile(fullPath, []byte(b.String()), 0755)
	if err != nil {
		return err
	}
	return nil
}
